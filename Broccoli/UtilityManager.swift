//
//  UtilityManager.swift
//  Broccoli
//
//  Created by Hadevs on 05/10/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UtilityManager: BaseManager {
    
    static var shared = UtilityManager()
    
    var banners: [Banner] = []
    var promos: [Promo] = []
    
    func loadPromos(with completion: @escaping () -> Void) {
        let headers = [
            "Authorization":"Basic ZGV2Ok9vUnJBYTIwMTde"
        ]
        
        let _ = Alamofire.request("https://test.freshbroccoli.ru/api/v1/main/promos", method: .get, headers: headers)
            .responseSwiftyJSON { (request, response, json, error) in
                
                completion()
                
                self.promos = json.map { Promo.from(json: $0.1) }
        }
    }
    
    func loadSavedPromos() {
        guard let arrayOfDicts = defaults.object(forKey: "promos") as? [[String : Any]] else {
            return
        }
        
        promos = arrayOfDicts.map { Promo.from(json: JSON($0)) }
    }
    
    func savePromos() {
        defaults.set(promos.map{$0.dict}, forKey: "promos")
        defaults.synchronize()
    }
    
    func loadBanners(with completion: @escaping () -> Void) {
        let headers = [
            "Authorization":"Basic ZGV2Ok9vUnJBYTIwMTde"
        ]
        
        let _ = Alamofire.request("https://test.freshbroccoli.ru/api/v1/main/banners", method: .get, headers: headers)
            .responseSwiftyJSON { (request, response, json, error) in
                completion()
                
                self.banners = json.map { Banner.from(json: $0.1) }
        }
    }
    
    func loadSavedBanners() {
        guard let arrayOfDict = defaults.object(forKey: "banners") as? [[String : Any]] else {
            return
        }
        
        banners = arrayOfDict.map { Banner.from(json: JSON($0)) }
    }
    
    func saveBanners() {
        defaults.set(banners.map{$0.dict}, forKey: "banners")
        defaults.synchronize()
    }
}
