//
//  CategoryManager.swift
//  Broccoli
//
//  Created by Hadevs on 04/10/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CategoryManager: BaseManager {
    static let sharedInstance = CategoryManager()
    
    var categories: [Category] = []
    
    func loadAllCategories(_ completion: @escaping () -> Void) {
        let headers = [
            "Authorization":"Basic ZGV2Ok9vUnJBYTIwMTde"
        ]
        
        let _ = Alamofire.request("https://test.freshbroccoli.ru/api/v1/categories", method: .get, headers: headers)
            .responseSwiftyJSON { (request, response, json, error) in
                self.categories = json.map { Category.from(JSON: $0.1) }
                completion()
        }
    }
    
    func saveCategories() {
        defaults.set(categories.map{$0.dict}, forKey: "categories")
        defaults.synchronize()
    }
    
    func loadSavedCategories() {
        guard let categories = defaults.object(forKey: "categories") as? [[String: Any]] else {
            return
        }
        
        self.categories = categories.map { Category.from(JSON: JSON($0)) }
    }
}
