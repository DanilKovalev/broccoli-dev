//
//  SearchViewController.swift
//  Broccoli
//
//  Created by Quaka on 18.09.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

public struct ProductsItem {
    var name: String
    var icon: UIImage
}

public let productItems: [ProductsItem] = [
    ProductsItem(name: "ОВОЩИ, ЗЕЛЕНЬ", icon: #imageLiteral(resourceName: "zelen")),
    ProductsItem(name: "ФРУКТЫ, ЯГОДЫ", icon: #imageLiteral(resourceName: "frukt")),
    ProductsItem(name: "МОЛОЧНЫЕ ПРОДУКТЫ", icon: #imageLiteral(resourceName: "moloko")),
    ProductsItem(name: "ОРЕХИ, СУХОФРУКТЫ", icon: #imageLiteral(resourceName: "orehi")),
    ProductsItem(name: "РЫБА, МОРЕПРОДУКТЫ", icon: #imageLiteral(resourceName: "riba")),
    ProductsItem(name: "МЯСО, ПТИЦА", icon: #imageLiteral(resourceName: "myaso")),
    ProductsItem(name: "СЛАДОСТИ", icon: #imageLiteral(resourceName: "sweet")),
    ProductsItem(name: "БАКАЛЕЯ", icon: #imageLiteral(resourceName: "hleb")),
    ProductsItem(name: "ПОДАРКИ", icon: #imageLiteral(resourceName: "podarki")),
    ProductsItem(name: "НАПИТКИ", icon: #imageLiteral(resourceName: "napitki")),
    ProductsItem(name: "ТОВАРЫ ДЛЯ ДЕТЕЙ", icon: #imageLiteral(resourceName: "tovariChildren")),
    ProductsItem(name: "БЫТОВАЯ ХИМИЯ", icon: #imageLiteral(resourceName: "himiya")),
    ProductsItem(name: "РАСПРОДАЖА", icon: #imageLiteral(resourceName: "procent")),
    ProductsItem(name: "ВСЕ ДЛЯ ГРИЛЯ", icon: #imageLiteral(resourceName: "grill"))
]

class SearchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var bar: UINavigationBar!
    @IBOutlet weak var categoriesSearchCollectionView: UICollectionView!
    @IBOutlet weak var productsSearchCollectionView: UICollectionView!
    @IBOutlet weak var searchProductsTextField: UITextField!
    
    let objects = ["Хит", "Огурцы", "Кабачки", "Бананы"]
    
    let cellIdentifier = "searchCategoryCell"
    let secondCellIdentifier = "searchProductCell"
    
    fileprivate func delegating() {
        // Do any additional setup after loading the view.
        
        categoriesSearchCollectionView.delegate   = self
        categoriesSearchCollectionView.dataSource = self
        
        productsSearchCollectionView.delegate     = self
        productsSearchCollectionView.dataSource   = self
    }
    
    fileprivate func registerNibs() {
        self.categoriesSearchCollectionView.register(UINib(nibName: "SearchCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        self.productsSearchCollectionView.register(UINib(nibName: "SearchProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: secondCellIdentifier)
    }
    
    fileprivate func addLeftViewToTextField() {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "lens"))
        imageView.sizeToFit()
        imageView.frame.size.width += 30
        imageView.contentMode = .scaleAspectFit
        searchProductsTextField.leftView = imageView
        searchProductsTextField.leftViewMode = .always
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addLeftViewToTextField()
        
        registerNibs()
        delegating()
        
        makeBarInvisible()
        
        configureCategoriesCollectionViewLayout()
        configureProductsCollectionViewLayout()
    }
    
    func configureCategoriesCollectionViewLayout() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 85, height: 30)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 25
        categoriesSearchCollectionView.collectionViewLayout = layout
        categoriesSearchCollectionView.showsHorizontalScrollIndicator = false
    }
    
    func configureProductsCollectionViewLayout() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 175, height: 120)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        productsSearchCollectionView.collectionViewLayout = layout
    }
    
    func makeBarInvisible() {
        bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        bar.shadowImage = UIImage()
        bar.isTranslucent = false
        bar.backgroundColor = UIColor.clear
        for parent in bar.subviews {
            for childView in parent.subviews where childView is UIImageView {
                    childView.removeFromSuperview()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoriesSearchCollectionView {
            return objects.count
        } else if collectionView == productsSearchCollectionView {
            return productItems.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
            case categoriesSearchCollectionView:
                
                guard let cell = collectionView.cellForItem(at: indexPath) as? SearchCollectionViewCell else {
                    return
                }
                
                cell.selectCell(!cell.isCustomSelected)
            default: break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == categoriesSearchCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! SearchCollectionViewCell
            cell.titleLabel.text = self.objects[indexPath.row]
            return cell
        } else if collectionView == productsSearchCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: secondCellIdentifier, for: indexPath) as! SearchProductCollectionViewCell
            cell.productLabel.text = productItems[indexPath.row].name
            cell.productImageView.image = productItems[indexPath.row].icon
            return cell
        }
        return UICollectionViewCell()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
