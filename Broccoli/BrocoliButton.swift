//
//  BrocoliButton.swift
//  Broccoli
//
//  Created by Oleg Chebotarev on 28.08.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class BrocoliButton: UIView {
    
    var borderWidth: CGFloat = 1.5
    static var radius: CGFloat = 10
    
    private static var GREEN_COLOR: UIColor {
        return UIColor(netHex: 0x8cad0f)
    }
    
    var leftTitle: String {
        didSet {
            firstLabel.text = leftTitle
        }
    }
    
    var rightTitle: String {
        didSet {
            secondLabel.text = rightTitle
        }
    }
    
    private var cartIconView: UIImageView!
    
    private var firstView: UIView!
    private var firstLabel: UILabel!
    
    private var secondView: UIView!
    private var secondLabel: UILabel!
    
    init(frame: CGRect, leftTitle: String, rightTitle: String) {
        self.leftTitle = leftTitle
        self.rightTitle = rightTitle
        super.init(frame: frame)
        
        initFirstView()
        initFirstLabel()
        initSecondView()
        initSecondLabel()
        initCartIconView()
        initActiveButton()
    }
    
    var activeButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = BrocoliButton.radius
        button.backgroundColor = BrocoliButton.GREEN_COLOR
        button.isHidden = true
        button.setTitle("В корзине", for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(#imageLiteral(resourceName: "cart"), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        button.titleLabel?.font = UIFont(name: "Lato-Regular", size: 14.0)
        button.isEnabled = false
        return button
    }()
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        activeButton.isHidden = !activeButton.isHidden
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let view = firstView {
            view.round(corners: [.bottomLeft, .topLeft], radius: BrocoliButton.radius, borderColor: BrocoliButton.GREEN_COLOR, borderWidth: self.borderWidth)
        }
        
        if let view = secondView {
            view.round(corners: [.topRight, .bottomRight], radius: BrocoliButton.radius, borderColor: BrocoliButton.GREEN_COLOR, borderWidth: self.borderWidth)
        }
        
        activeButton.frame = bounds
    }
    
    func initActiveButton() {
        addSubview(activeButton)
    }
    
    func initCartIconView() {
        cartIconView = UIImageView(image: #imageLiteral(resourceName: "cart"))
        cartIconView.translatesAutoresizingMaskIntoConstraints = false
        cartIconView.contentMode = .scaleAspectFit
        
        addSubview(cartIconView)
        
        let verticalMargin: CGFloat = 18
        
        let top = NSLayoutConstraint(item: cartIconView, attribute: .topMargin, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: verticalMargin)
        let bottom = NSLayoutConstraint(item: cartIconView, attribute: .bottomMargin, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -1 * verticalMargin)
        let leading = NSLayoutConstraint(item: cartIconView, attribute: .leftMargin, relatedBy: .equal, toItem: firstView, attribute: .right, multiplier: 1, constant: 4)
        let trailing = NSLayoutConstraint(item: cartIconView, attribute: .trailingMargin, relatedBy: .equal, toItem: secondLabel, attribute: .leading, multiplier: 1, constant: 0)

        addConstraints([top, bottom, leading, trailing])
    }
    
    func initSecondLabel() {
        secondLabel = UILabel()
        secondLabel.translatesAutoresizingMaskIntoConstraints = false
        secondLabel.textColor = .white
        secondLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        secondLabel.text = rightTitle
        secondLabel.backgroundColor = .clear
        secondLabel.textAlignment = .center
        
        addSubview(secondLabel)
        
        let top = NSLayoutConstraint(item: secondLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: secondLabel, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: secondLabel, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -8)
        let width = NSLayoutConstraint(item: secondLabel, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0.38, constant: 0)
        
        addConstraints([top, bottom, trailing, width])
    }
    
    func initFirstLabel() {
        firstLabel = UILabel()
        firstLabel.translatesAutoresizingMaskIntoConstraints = false
        firstLabel.textColor = BrocoliButton.GREEN_COLOR
        firstLabel.font = UIFont.boldSystemFont(ofSize: 10.0)
        firstLabel.adjustsFontSizeToFitWidth = true
        firstLabel.minimumScaleFactor = 0.2
        firstLabel.text = leftTitle
        firstLabel.backgroundColor = .clear
        firstLabel.textAlignment = .center
        firstLabel.numberOfLines = 2
        addSubview(firstLabel)
        
        let top = NSLayoutConstraint(item: firstLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: firstLabel, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: firstLabel, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 4)
        let width = NSLayoutConstraint(item: firstLabel, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0.34, constant: 0)
        
        addConstraints([top, bottom, leading, width])
    }
    
    func initSecondView() {
        secondView = UIView()
        secondView.translatesAutoresizingMaskIntoConstraints = false
        secondView.layer.borderColor = BrocoliButton.GREEN_COLOR.cgColor
        secondView.layer.borderWidth = borderWidth
        secondView.backgroundColor = BrocoliButton.GREEN_COLOR
        
        addSubview(secondView)
        
        let leading = NSLayoutConstraint(item: secondView, attribute: .leading, relatedBy: .equal, toItem: firstView, attribute: .trailing, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: secondView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        let top = NSLayoutConstraint(item: secondView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: secondView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        
        addConstraints([leading, trailing, top, bottom])
    }
    
    func initFirstView() {
        firstView = UIView()
        firstView.translatesAutoresizingMaskIntoConstraints = false
        firstView.layer.borderColor = BrocoliButton.GREEN_COLOR.cgColor
        firstView.layer.borderWidth = borderWidth
        firstView.backgroundColor = .clear
        
        addSubview(firstView)
        
        let top = NSLayoutConstraint(item: firstView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: firstView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: firstView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let width = NSLayoutConstraint(item: firstView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0.4, constant: 0)
        
        addConstraints([top, bottom, leading, width])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UIView {
    
    /**
     Rounds the given set of corners to the specified radius
     
     - parameter corners: Corners to round
     - parameter radius:  Radius to round to
     */
    func round(corners: UIRectCorner, radius: CGFloat) {
        _ = _round(corners: corners, radius: radius)
    }
    
    /**
     Rounds the given set of corners to the specified radius with a border
     
     - parameter corners:     Corners to round
     - parameter radius:      Radius to round to
     - parameter borderColor: The border color
     - parameter borderWidth: The border width
     */
    func round(corners: UIRectCorner, radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        let mask = _round(corners: corners, radius: radius)
        addBorder(mask: mask, borderColor: borderColor, borderWidth: borderWidth)
    }
    
    /**
     Fully rounds an autolayout view (e.g. one with no known frame) with the given diameter and border
     
     - parameter diameter:    The view's diameter
     - parameter borderColor: The border color
     - parameter borderWidth: The border width
     */
    func fullyRound(diameter: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        layer.masksToBounds = true
        layer.cornerRadius = diameter / 2
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor;
    }
    
}

private extension UIView {
    
    @discardableResult func _round(corners: UIRectCorner, radius: CGFloat) -> CAShapeLayer {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        return mask
    }
    
    func addBorder(mask: CAShapeLayer, borderColor: UIColor, borderWidth: CGFloat) {
        let borderLayer = CAShapeLayer()
        borderLayer.path = mask.path
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = borderColor.cgColor
        borderLayer.lineWidth = borderWidth
        borderLayer.frame = bounds
        layer.addSublayer(borderLayer)
    }
    
}
