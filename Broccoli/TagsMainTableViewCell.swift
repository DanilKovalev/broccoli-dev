//
//  TagsMainTableViewCell.swift
//  Broccoli
//
//  Created by 1 on 05/10/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit


class TagsMainTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var tagsCollectionView: UICollectionView!
    
    var tags:[String] = []
    
    var chosenTags: [Int] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tags = TagManager.sharedInstance.getAllTagNames()
        registeringCells()
        configureCategoriesCollectionViewLayout()
        addObservers()
        tagsCollectionView.delegate = self
        tagsCollectionView.dataSource = self
        // Initialization code
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagMainCollectionViewID", for: indexPath) as! TagMainCollectionViewCell
        cell.tagName.text = tags[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? TagMainCollectionViewCell else {
            return

        }
        cell.selectCell(!cell.isCustomSelected)
        let newTag = TagManager.sharedInstance.id(by:tags[indexPath.row])
        if chosenTags.contains(newTag) {
            chosenTags.remove(at: chosenTags.index(of: newTag)!)
        } else {
            chosenTags.append(newTag)
        }
        let notification = Notification(name: Notification.Name.init("mainTagsUpdated"), object: self, userInfo: ["tags": chosenTags])
        NotificationCenter.default.post(notification)
        
        print("sosa")
   
        if chosenTags.isEmpty {
//                if tagSavedProducts {
//                    productsMain = savedProducts
//                    tagSavedProducts = false
//                } else {
//                    savedProducts = products
//                    tagSavedProducts = true
//                }
//            }
//
//            if chosenTags.contains(newTag){
//                products = savedProducts
//                chosenTags.remove(at: chosenTags.index(of: newTag)!)
//            } else {
//                chosenTags.append(newTag)
//            }
        
        }
    }
    
    func registeringCells() {
        let nib = UINib(nibName: "TagMainCollectionViewCell", bundle: nil)
        tagsCollectionView.register(nib, forCellWithReuseIdentifier: "TagMainCollectionViewID")
    }
    
    fileprivate func configureCategoriesCollectionViewLayout() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 85, height: 35)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 25
        tagsCollectionView.collectionViewLayout = layout
        tagsCollectionView.showsHorizontalScrollIndicator = false
    }
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(tagsUpdated), name: NSNotification.Name(rawValue: "tagsUpdated"), object: nil)
    }
    func tagsUpdated() {
        tags = TagManager.sharedInstance.getAllTagNames()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
