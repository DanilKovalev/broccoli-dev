//
//  Promo.swift
//  Broccoli
//
//  Created by Hadevs on 08/10/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Promo {
    var id: Int
    var active: Bool
    var detailPicture: String // url
    var sort: Int
    var name: String
    var badge = Badge(name: "", color: "")
    var products: [Product] = []
    
    var dict: [String: Any] {
        return [
            "id": id,
            "active": active,
            "detailPicture": detailPicture,
            "sort": sort,
            "name": name,
            "badge": badge.dict,
            "products": products.map {$0.dict}
        ]
    }
    
    static func from(json: JSON) -> Promo {
        return Promo(id: json["id"].intValue,
                     active: json["active"].boolValue,
                     detailPicture: json["detailPicture"].stringValue,
                     sort: json["sort"].intValue,
                     name: json["name"].stringValue,
                     badge: Badge.from(json: json["badge"]),
                     products: json["products"].map{ Product.from(JSON: $0.1) }
            )
    }
}

struct Badge {
    var name: String
    var color: String // in HEX
    
    var dict: [String: Any] {
        return [
            "name": name,
            "color": color
        ]
    }
    
    static func from(json: JSON) -> Badge {
        return Badge(name: json["name"].stringValue,
                     color: json["color"].stringValue)
    }
}
