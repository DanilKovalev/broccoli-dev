//
//  RegisterManager.swift
//  Broccoli
//
//  Created by Fedor Emelianov on 28.08.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import Alamofire

class RegisterManager: BaseManager {
    static let sharedInstance = RegisterManager()
    
    func register(firstName: String, lastName: String, phone: String, email: String, password: String, completion:  @escaping (NSDictionary?, String?)->Void = {_ in }) {
        let parameters: Parameters = [
            "firstName": firstName,
            "lastName": lastName,
            "email": email,
            "phone": phone,
            "password": password
        ]
        request(path: "auth/signUp", parameters: parameters, completion: completion)
    }
    
    func confirm(phone: String, code: String, completion:  @escaping (NSDictionary?, String?)->Void = {_ in }) {
        let parameters: Parameters = [
            "phone": phone,
            "acceptCode": code
        ]
        request(path: "auth/acceptCode/check", parameters: parameters, completion: completion)
    }
}
