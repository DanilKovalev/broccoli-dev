//
//  ProductInfo.swift
//  Broccoli
//
//  Created by Hadevs on 18/09/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import SwiftyJSON

struct ProductInfo {
    var detailText: String?
    var keepDescription: String?
    var benefitDescription: String?
    
    var dict: [String: Any?] {
         return [
            "detailText": detailText ?? "",
            "keepDescription": keepDescription ?? "",
            "benefitDescription": benefitDescription ?? ""
        ]
    }
    
    static func from(JSON json: JSON) -> ProductInfo {
        return ProductInfo(detailText: json["detailText"].string,
                           keepDescription: json["keepDescription"].string,
                           benefitDescription: json["benefitDescription"].string)
    }
}
