//
//  CatalogTagsCollectionViewCell.swift
//  Broccoli
//
//  Created by Nikita on 20/09/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class CatalogTagsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var tagName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tagName.layer.borderColor = UIColor.orange.cgColor
        tagName.layer.borderWidth = 1.0
    }

}
