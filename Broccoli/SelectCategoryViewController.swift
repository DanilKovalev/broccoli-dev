
//
//  SelectCategoryViewController.swift
//  Broccoli
//
//  Created by 1 on 18/09/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class SelectCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var CategoryTableView: UITableView!
    
    var categories: [Category] = [] {
        didSet {
            CategoryTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registeringCells()
        
        CategoryTableView.dataSource = self
        CategoryTableView.delegate = self
        // Do any additional setup after loading the view.
        
        loadCategories()
        addObservers()
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(categoriesUpdated), name: NSNotification.Name.init("categoriesUpdated"), object: nil)
    }
    
    func categoriesUpdated() {
        loadCategories()
    }
    
    func loadCategories() {
        categories = CategoryManager.sharedInstance.categories.filter {
            $0.childs.isEmpty == false
        }
    }
    
    func registeringCells() {
        CategoryTableView.register(UINib(nibName: "ChooseCategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "ChooseCategoryID")
        CategoryTableView.register(UINib(nibName: "AllCategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "AllCategoryID")
        CategoryTableView.register(UINib(nibName: "SelectCategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectCategoryID")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let category = categories[indexPath.row - 2]
        let products = CategoryManager.sharedInstance.categories.filter {
            category.childs.contains(Int($0.id)!)
        }
        
        let vc = SelectProductViewController(nibName: "SelectProductViewController", bundle: nil)
        vc.categories = products
        vc.mainCategory = category
        
        present(vc, animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseCategoryID", for: indexPath) as! ChooseCategoryTableViewCell
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AllCategoryID", for: indexPath) as! AllCategoryTableViewCell
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCategoryID", for: indexPath) as! SelectCategoryTableViewCell
            
            let category = categories[indexPath.row - 2]
            cell.configure(with: category)

            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count + 2
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
