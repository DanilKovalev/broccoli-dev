//
//  BaseManager.swift
//  Broccoli
//
//  Created by Oleg Chebotarev on 28.08.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BaseManager: NSObject {
    
    func baseUrl() -> String {
        return "https://test.freshbroccoli.ru/api/v1/"
    }
    
    func request(path: String, parameters: Parameters, method: HTTPMethod = .post, completion: @escaping (NSDictionary?, String?)->Void = {_ in }) {
        let headers = [
            "Authorization":"Basic ZGV2Ok9vUnJBYTIwMTde"
        ]
        
        Alamofire.request(self.baseUrl() + path, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                
                switch response.result {
                case .success(let JSON):
                    if let routingNumber = JSON as? NSNull {
                        completion(nil, "Ошибка. Попробуйте позднее")
                        return
                    }
                    print("Success with JSON: \(JSON)")
                    
                    let response = JSON as! NSDictionary
                    if(response.object(forKey: "error") != nil) {
                        completion(nil, response.object(forKey: "error") as? String)
                    }
                    completion(response, nil)
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    var errorMessage = "Ошибка. Попробуйте позднее"
                    
                    if let data = response.data {
                        let responseJSON = JSON(data: data)
                        
                        let message: String = responseJSON["error"].stringValue
                            if !message.isEmpty {
                                errorMessage = message
                            } 
                    }
                    completion(nil, errorMessage)
                }
        }
    }
}
