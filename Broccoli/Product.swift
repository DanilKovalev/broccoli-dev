//
//  Product.swift
//  Broccoli
//
//  Created by Hadevs on 18/09/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct Product {
    
    var id: Int
    var active: Bool
    var detailPicture: String?
    var sort: Int?
    var name: String?
    var weight: String?
    var rating: Int?
    var shareLink: String?
    var morePhoto: [String] = []
    var productInfo: ProductInfo?
    var commerce:[ProductCommerce] = []
    var detailImage: UIImage?
    var manufacturer: Int? // ID Производителя
    var badge: (name: String?, color: String?)? // Бейдж http://prntscr.com/g6op1z, цвет в HEX
    var tags: [Int] = []
    var stickers: [Int] = []
    
    var dict: [String: Any?] {
        return [
            "id": id,
            "active": active,
            "detailPicture": detailPicture ?? "",
            "sort": sort ?? "",
            "name": name ?? "",
            "weight": weight ?? "",
            "rating": rating ?? "",
            "shareLink": shareLink ?? "",
            "morePhoto": morePhoto,
            "productInfo": productInfo == nil ? [:] : productInfo!.dict,
            "commerce": commerce.map{$0.dict},
            "manufacturer": manufacturer ?? -1,
            "badge": badge == nil ? [:] : ["name": "", "color": ""],
            "tags": tags,
            "stickers": stickers
        ]
    }
    
    static func from(JSON json: JSON) -> Product {
        return Product(id: json["id"].intValue,
                       active: json["active"].boolValue,
                       detailPicture: json["detailPicture"].string,
                       sort: json["sort"].int,
                       name: json["name"].string,
                       weight: json["weight"].string,
                       rating: json["rating"].int,
                       shareLink: json["shareLink"].string,
                       morePhoto: (json["morePhoto"].arrayObject as? [String]) ?? [],
                       productInfo: ProductInfo.from(JSON: json["productInfo"]),
                       commerce: json["commerce"].map{ProductCommerce.from(JSON: $0.1)},
                       detailImage: nil,
                       manufacturer: json["manufacturer"].int,
                       badge: (name: json["badge"].string, color: json["badge"].string),
                       tags: (json["tags"].arrayObject as? [Int]) ?? [],
                       stickers: (json["stickers"].arrayObject as? [Int]) ?? [])
    }
    
    var isValid: Bool {return id != Int.max && active == true}
    
    mutating func getDetailLocalPhoto() -> UIImage? {
        guard let data = defaults.data(forKey: "image_\(id)") else {
            return nil
        }
        
        let image = UIImage(data: data)
        self.detailImage = image
        
        return image
    }
    
    func saveDetail(photo: UIImage) {
        DispatchQueue.global(qos: .utility).async {
            guard let data = UIImagePNGRepresentation(photo) else {
                return
            }
            
            defaults.set(data, forKey: "image_\(self.id)")
            defaults.synchronize()
        }
    }
}
