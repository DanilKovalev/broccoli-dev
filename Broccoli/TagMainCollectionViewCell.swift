//
//  CollectionViewCell.swift
//  Broccoli
//
//  Created by 1 on 05/10/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class TagMainCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var tagName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tagName.layer.borderColor = UIColor.gray.cgColor
        tagName.layer.borderWidth = 1.0
        tagName.layer.cornerRadius = 5
        // Initialization code
    }
    var isCustomSelected = false
    
    func selectCell(_ bool: Bool) {
        
        isCustomSelected = bool
        
        if bool {
            tagName.layer.borderColor = UIColor.orange.cgColor
            tagName.textColor = UIColor.orange
        } else {
            tagName.layer.borderColor = UIColor(netHex: 0x7A7B93).withAlphaComponent(0.6).cgColor
            tagName.textColor = UIColor(netHex: 0x7A7B93)
        }
    }

}
