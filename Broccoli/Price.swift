//
//  Price.swift
//  Broccoli
//
//  Created by Hadevs on 22/09/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Price {
    var typeId: Int
    var oldPrice: Int?
    
    var values: [PriceValue] = []
    
    var dict: [String: Any?] {
        return [
            "typeId": typeId,
            "oldPrice": oldPrice,
            "values": values.map{$0.dict}
        ]
    }
    
    static func from(json: JSON) -> Price {
        return Price(typeId: json["typeId"].intValue,
                     oldPrice: json["oldPrice"].int,
                     values: json["values"].map{PriceValue.from(json: $0.1)})
    }
}

struct PriceValue {
    var priceId: Int?
    var purchasingCurrency: String?
    var price: Int?
    var quantityFrom: Int?
    var quantityTo: Int?
    
    static func from(json: JSON) -> PriceValue {
        return PriceValue(priceId: json["priceId"].int,
                          purchasingCurrency: json["purchasingCurrency"].string,
                          price: json["price"].int,
                          quantityFrom: json["quantityFrom"].int,
                          quantityTo: json["quantityTo"].int)
    }
    
    var dict: [String: Any?] {
        return [
            "priceId": priceId ?? 0,
            "purchasingCurrency": purchasingCurrency ?? "",
            "price": price ?? 0,
            "quantityFrom": quantityFrom ?? 0,
            "quantityTo": quantityTo ?? Int.max
        ]
    }
}
