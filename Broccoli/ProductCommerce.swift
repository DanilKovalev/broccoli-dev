//
//  ProductCommerce.swift
//  Broccoli
//
//  Created by Hadevs on 18/09/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import SwiftyJSON

struct ProductCommerce {
    var id: Int?
    var good_id: Int?
    var detailPicture: String?
    var morePhoto: [String] = []
    var productInfo: String?
    var measureId: Int?
    var measureRatio: Int?
    var props: [Prop] = []
    
    var dict: [String: Any?] {
        return [
            "id": id ?? -1,
            "good_id": good_id ?? -1,
            "detailPicture": detailPicture ?? "",
            "morePhoto": morePhoto,
            "productInfo": productInfo ?? "",
            "measureId": measureId ?? -1,
            "measureRatio": measureRatio ?? -1,
            "props": props.map{$0.dict}
        ]
    }
    
    static func from(JSON json: JSON) -> ProductCommerce {
        return ProductCommerce(id: json["id"].int,
                               good_id: json["good_id"].int,
                               detailPicture: json["detailPicture"].string,
                               morePhoto: (json["morePhoto"].arrayObject as? [String]) ?? [],
                               productInfo: json["productInfo"].string,
                               measureId: json["measureId"].int,
                               measureRatio: json["measureRatio"].int,
                               props: json["props"].map{Prop.from(JSON: $0.1)})
    }
}

struct Prop {
    var title: String?
    var code: String?
    var value: String?
    
    var dict: [String: Any?] {
        return [
            "title" : title ?? "",
            "code" : code ?? "",
            "value": value ?? "",
        ]
    }
    
    static func from(JSON json: JSON) -> Prop {
        return Prop(title: json["title"].string,
                    code: json["code"].string,
                    value: json["value"].string)
    }
}
