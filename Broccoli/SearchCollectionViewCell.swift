//
//  SearchCollectionViewCell.swift
//  Broccoli
//
//  Created by Quaka on 18.09.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class SearchCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLabel.layer.masksToBounds = true
        titleLabel.backgroundColor = .white
        titleLabel.layer.borderWidth = 1.0
        titleLabel.layer.borderColor = UIColor(netHex: 0x7A7B93).withAlphaComponent(0.6).cgColor
    }
    
    var isCustomSelected = false
    
    func selectCell(_ bool: Bool) {
        
        isCustomSelected = bool
        
        if bool {
            titleLabel.layer.borderColor = UIColor.orange.cgColor
            titleLabel.textColor = UIColor.orange
        } else {
            titleLabel.layer.borderColor = UIColor(netHex: 0x7A7B93).withAlphaComponent(0.6).cgColor
            titleLabel.textColor = UIColor(netHex: 0x7A7B93)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        titleLabel.layer.cornerRadius = 4
    }

}

