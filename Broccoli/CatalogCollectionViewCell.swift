//
//  CatalogCollectionViewCell.swift
//  Broccoli
//
//  Created by Nikita on 18/09/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class CatalogCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var like: UIButton!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var price: UIButton!
    @IBOutlet weak var badgeView: UIImageView!
    
    var addButton: BrocoliButton!
    var imageTask: URLSessionDataTask!
    private var product: Product!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        if let task = imageTask {
            task.cancel()
        }
        
        image.image = nil
        weight.text = nil
        addButton = BrocoliButton(frame: price.frame, leftTitle: "... кг", rightTitle: "0 р.")
        badgeView.image = nil
        
        super.prepareForReuse()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        like.layer.cornerRadius   = 3
        image.layer.cornerRadius  = 3
        image.layer.masksToBounds = true
        name.sizeToFit()
        name.numberOfLines        = 2
        image.backgroundColor     =  UIColor(netHex: 0xf1f1f1)
        price.isHidden            = true
        badgeView.isHidden        = true
        
        initButton()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init("productsLoaded"), object: nil, queue: nil) { (_) in
            let price = PriceManager.sharedInstance.getPrice(for: self.product, andCount: 0) ?? 0
            
            self.addButton.rightTitle = "\(price) р."
            self.addButton.leftTitle  = "\(self.product.weight ?? "... г")"
        }
    }
    
    func initButton() {
        addButton = BrocoliButton(frame: price.frame, leftTitle: "... кг", rightTitle: "0 р.")
        addSubview(addButton)
    }
    
     func configureBy(product: Product) {
        self.product = product
        
        name.text = product.name
        weight.text = product.weight
        
        let price = PriceManager.sharedInstance.getPrice(for: product, andCount: 0) ?? 0

        addButton.rightTitle = "\(price) р."
        addButton.leftTitle  = "\(product.weight ?? "... г")"
        
        loadPhoto(by: product)

    }
    
    func compressImage (_ image: UIImage) -> UIImage {
        
        let actualHeight:CGFloat = image.size.height
        let actualWidth:CGFloat = image.size.width
        let imgRatio:CGFloat = actualWidth/actualHeight
        let maxWidth:CGFloat = 200
        let resizedHeight:CGFloat = maxWidth/imgRatio
        let compressionQuality:CGFloat = 0.6
        
        let rect:CGRect = CGRect(x: 0, y: 0, width: maxWidth, height: resizedHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData:Data = UIImageJPEGRepresentation(img, compressionQuality)!
        UIGraphicsEndImageContext()
        
        return UIImage(data: imageData)!
        
    }
    
    func loadPhoto(by product: Product) {
        self.image.image = nil
        if let image = self.product.detailImage {
            self.image.image = image
        } else if let image = self.product.getDetailLocalPhoto() {
            self.image.image = image
        } else {
            if let url = URL(string: product.detailPicture ?? "") {
                loadPhoto(by: url, withCompletion: { downloadedImage in
                    if downloadedImage != nil {
                        let image = self.compressImage(downloadedImage!)
                        self.image.image = image
                        self.product.saveDetail(photo: image)
                    }
                })
            }
        }
    }
    
    fileprivate func loadPhoto(by url: URL, withCompletion completion: @escaping (UIImage?) -> Void) {
        
        var request = URLRequest(url: url)
        request.setValue("Basic ZGV2Ok9vUnJBYTIwMTde", forHTTPHeaderField: "Authorization")
        
        imageTask = URLSession.shared.dataTask(with: request) { data, response, error in
            if data != nil, let photo = UIImage(data: data!) {
                DispatchQueue.main.async {
                    completion(photo)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
        
        imageTask.resume()
    }
    
    
    fileprivate func animate(withView view: UIView, _ completion: @escaping () -> Void) {
        UIView.transition(with: view, duration: 0.1, options: .transitionCrossDissolve, animations: {
            completion()
        }, completion: nil)
    }
    
//    fileprivate func loadBage(by product: Product) {
//        badgeView.image = nil
//
//        product.badge
//    }
   

}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in PNG format
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the PNG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    var png: Data? { return UIImagePNGRepresentation(self) }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
}
