//
//  PriceManager.swift
//  Broccoli
//
//  Created by Hadevs on 22/09/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PriceManager: BaseManager {
    static let sharedInstance = PriceManager()

    var prices: [String: [Price]] = [:]
    
    func loadAllPrices(withCompletion completion: (() -> Void)?, offset: Int = 0, limit: Int = 500) {
        let headers = [
            "Authorization":"Basic ZGV2Ok9vUnJBYTIwMTde"
        ]
      
        let url = "\(baseUrl())products/prices?offset=\(offset)&limit=\(limit)"
        
        let _ = Alamofire.request(url, method: .get, headers: headers)
            .responseSwiftyJSON { (request, response, json, error) in
                
                if error == nil {
                    
                    json.forEach({ (arg) in
                        let (key, value) = arg
                        self.prices[key] = value["prices"].map{Price.from(json: $0.1)}
                    })

                    if json.count == 0 {
                        if let completion = completion {
                            completion()
                        }
                        
                        self.savePrices()
                    } else {
                        self.loadAllPrices(withCompletion: {
                            completion?()
                        }, offset: offset + 501, limit: limit + 500)
                    }
            }
        }
    }

    
    func savePrices() {
        var dict: [String: Any] = [:]
        
        prices.forEach { (key,value) in
            dict[key] = value.map{$0.dict}
        }
        
        defaults.set(dict, forKey: "prices")
        defaults.synchronize()
    }
    
    func loadSavedPrices() {
        guard let dict = defaults.object(forKey: "prices") as? [String: Any] else {
            return
        }
        
        dict.forEach { (key,value) in
            let array = value as! [[String: Any]]
            self.prices[key] = array.map({ (dict) -> Price in
                return Price.from(json: JSON(dict))
            })
        }
}
    
    func getPrice(for product: Product, andCount count: Int) -> Int? {
        guard let prices = self.prices["\(product.id)"] else {
            return nil
        }
        
//        print("price: \(product.name) -> \(prices)")
        
        let firstPriceElement = prices[0]
        let pricesForQuantity = firstPriceElement.values.filter { (value) -> Bool in
            return value.quantityFrom ?? 0 >= count && count <= value.quantityTo ?? Int.max
        }
        
        if pricesForQuantity.count > 0 {
            return pricesForQuantity[0].price
        } else {
            return nil
        }
    }
}
