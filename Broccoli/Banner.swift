//
//  Banner.swift
//  Broccoli
//
//  Created by Hadevs on 05/10/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import SwiftyJSON

class Banner: NSObject {
    var id: Int
    var active: Bool
    var detailPicture: String? // url
    var sort: Int?
    
    init(id: Int, active: Bool, detailPicture: String?, sort: Int?) {
        self.id = id
        self.active = active
        self.detailPicture = detailPicture
        self.sort = sort
    }
    
    var dict : [String: Any] {
        return [
            "id": id,
            "active": active,
            "detailPicture": detailPicture ?? "",
            "sort": sort ?? 0
        ]
    }
    
    static func from(json: JSON) -> Banner {
        
        return Banner(id: json["id"].intValue,
                      active: json["active"].boolValue,
                      detailPicture: json["detailPicture"].stringValue,
                      sort: json["sort"].intValue)
        
    }
    
}
