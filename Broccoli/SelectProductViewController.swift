//
//  SelectProductViewController.swift
//  Broccoli
//
//  Created by 1 on 18/09/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class SelectProductViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var productTableView: UITableView!
    
    var mainCategory: Category!
    var categories: [Category] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        productTableView.delegate = self
        productTableView.dataSource = self
        
        registeringCells()
        
        // Do any additional setup after loading the view.
    }
	
    
    func registeringCells() {
        productTableView.register(UINib(nibName: "SelectProductSearchTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectProductSearchID")
        productTableView.register(UINib(nibName: "SelectProductAllTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectProductAllTableViewID")
        productTableView.register(UINib(nibName: "SelectProductTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectProductID")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard indexPath.row > 0 else {
            return
        }
        
        var products: [Product]!
        if  indexPath.row == 1 {
            products = mainCategory.products
        } else {
            products = categories[indexPath.row - 2].products
        }
        
        let catalogVC = CatalogViewController(nibName: "CatalogViewController", bundle: nil)
        catalogVC.set(products: products)
        present(catalogVC, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        productTableView.rowHeight = 70
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectProductSearchID", for: indexPath) as! SelectProductSearchTableViewCell
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectProductAllTableViewID", for: indexPath) as! SelectProductAllTableViewCell
            return cell
        } else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectProductID", for: indexPath) as! SelectProductTableViewCell
            
            let category = categories[indexPath.row - 2]
            cell.configure(with: category)
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return categories.count + 2
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
