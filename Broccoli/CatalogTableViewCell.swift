//
//  CatalogTableViewCell.swift
//  Broccoli
//
//  Created by 1 on 05/10/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class CatalogTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var catalogCollectionView: UICollectionView!
    
    var products: [Product]         = mainProducts
    var savedProducts: [Product]    = []
    var savedProductsBool           = true
    var tagSavedProducts            = false
    var tagCounter = 0
    var chosenTags:[Int] = [] {
        didSet {
            if tagCounter == 0 {
                self.savedProducts = products
                
                }
            if chosenTags.isEmpty && tagCounter != 0 {
              
                products = savedProducts
                tagCounter = 0
                
            }
                if !chosenTags.isEmpty {
                    tagCounter = tagCounter + 1
                    self.products = products.filter {
                        var result = false
                            for tag in chosenTags {
                                if $0.tags.contains(tag) {
                                    result = true
                                    }
                                    }
                                    return result
                                }
                            }
            DispatchQueue.main.async {
                self.catalogCollectionView.reloadData()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        registeringCells()
        configureCategoriesCollectionViewLayout()
        
        catalogCollectionView.dataSource = self
        catalogCollectionView.delegate = self
        addObservers()
    
        catalogCollectionView.isScrollEnabled = true
        // Initialization code
    }
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(productsUpdated), name: NSNotification.Name(rawValue: "productsUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(tagsUpdated(sender:)), name: NSNotification.Name(rawValue: "mainTagsUpdated"), object: nil)
    }
    
    func tagsUpdated(sender: Notification) {
        let info = sender.userInfo
        let gettedTags = info!["tags"] as! [Int]
        
        chosenTags = gettedTags
        print("check")
        print(gettedTags)
    }
    
    func productsUpdated() {
        
        guard products.count == 0 else {
            return
        }
        
        var counter = 0
        
        for product in mainProducts {
            products.append(product)
            DispatchQueue.main.async {
                self.catalogCollectionView.reloadData()
            }
           // if isPaginationEnabled && counter == 9 { break; }
            counter += 1
        }
        
        JustHUD.shared.hide()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatalogMainCollectionViewID", for: indexPath) as! CatalogMainCollectionViewCell
        let product = products[indexPath.row]
        cell.layer.shouldRasterize = true
        cell.layer.rasterizationScale = UIScreen.main.scale
        cell.configureBy(product: product)
        return cell
    }
    
    func registeringCells() {
        let nib = UINib(nibName: "CatalogMainCollectionViewCell", bundle: nil)
        catalogCollectionView.register(nib, forCellWithReuseIdentifier: "CatalogMainCollectionViewID")
    }
    
    
    fileprivate func configureCategoriesCollectionViewLayout() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 186, height: 320)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 0)
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 25
        catalogCollectionView.collectionViewLayout = layout
        catalogCollectionView.showsHorizontalScrollIndicator = false
    }
    
}
