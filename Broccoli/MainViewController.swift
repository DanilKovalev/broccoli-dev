//
//  MainViewController.swift
//  Broccoli
//
//  Created by 1 on 04/10/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var mainTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registeringCells()
        
        mainTableView.dataSource = self
        mainTableView.delegate = self
        
        mainTableView.separatorInset = UIEdgeInsets(top: 0, left: 15000, bottom: 0, right: 0)
        // Do any additional setup after loading the view.
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = mainTableView.dequeueReusableCell(withIdentifier: "SearchTableViewID", for: indexPath) as! SearchTableViewCell
            
            let imageView = UIImageView(image: #imageLiteral(resourceName: "lens"))
            imageView.sizeToFit()
            imageView.frame.size.width += 65
            imageView.contentMode = .scaleAspectFit
            cell.searchTextField.leftView = imageView
            cell.searchTextField.leftViewMode = .always
            
            return cell
        } else if indexPath.row == 1 {
            
            let cell = mainTableView.dequeueReusableCell(withIdentifier: "AdImageTableViewID", for: indexPath) as! AdImageTableViewCell
            return cell
        } else if indexPath.row == 2 {
            let cell = mainTableView.dequeueReusableCell(withIdentifier: "ChooseCategoryMainTableViewID", for: indexPath) as! ChooseCategoryMainTableViewCell
            return cell
        } else if indexPath.row == 3 {
            let cell = mainTableView.dequeueReusableCell(withIdentifier: "ExtraCategoryTableViewID", for: indexPath) as! ExtraCategoryTableViewCell
            
            cell.promoSelected = {
                vc in
                
                self.present(vc, animated: true, completion: nil)
            }
            
            return cell
        } else if indexPath.row == 4 {
            let cell = mainTableView.dequeueReusableCell(withIdentifier: "TagsMainTableViewID", for: indexPath) as! TagsMainTableViewCell
         
            return cell
        } else if indexPath.row == 5 {
            let cell = mainTableView.dequeueReusableCell(withIdentifier: "CatalogTableViewID", for: indexPath) as! CatalogTableViewCell
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 70
        } else if indexPath.row == 1 {
            return 600
        } else if indexPath.row == 2 {
            return 80
        } else if indexPath.row == 3 {
            return 180
        } else if indexPath.row == 4 {
            return 70
        } else if indexPath.row == 5 {
            return 740
        } else {
            return 100
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
            let vc = SelectCategoryViewController(nibName: "SelectCategoryViewController", bundle: nil)
            present(vc, animated: false, completion: nil)
        }
    }
    
    func registeringCells() {
        mainTableView.register(UINib(nibName: "ChooseCategoryMainTableViewCell", bundle: nil), forCellReuseIdentifier: "ChooseCategoryMainTableViewID")
        mainTableView.register(UINib(nibName: "AdImageTableViewCell", bundle: nil), forCellReuseIdentifier: "AdImageTableViewID")
        mainTableView.register(UINib(nibName: "SearchTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchTableViewID")
        mainTableView.register(UINib(nibName: "TagsMainTableViewCell", bundle: nil), forCellReuseIdentifier: "TagsMainTableViewID")
        mainTableView.register(UINib(nibName: "ExtraCategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "ExtraCategoryTableViewID")
        mainTableView.register(UINib(nibName: "CatalogTableViewCell", bundle: nil), forCellReuseIdentifier: "CatalogTableViewID")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
