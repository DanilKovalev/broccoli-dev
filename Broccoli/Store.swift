//
//  Store.swift
//  Broccoli
//
//  Created by Oleg Chebotarev on 05.09.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class Store: NSObject {
    static var sharedInstance: Store = Store()
    
    func saveString(key: String, value: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    
    func getString(key:String) -> String {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: key)!
    }
    
    func saveBool(key: String, value: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    
    func getBool(key:String) -> Bool {
        let defaults = UserDefaults.standard
        return defaults.bool(forKey: key)
    }
}
