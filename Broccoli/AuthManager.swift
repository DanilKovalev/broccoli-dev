//
//  AuthManager.swift
//  Broccoli
//
//  Created by Oleg Chebotarev on 26.08.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import Alamofire

class AuthManager: BaseManager {
    static let sharedInstance = AuthManager()
    
    func auth(login: String, password: String, completion: @escaping (NSDictionary?, String?)->Void = {_ in }) {
        let parameters: Parameters = [
            "email": login,
            "password": password
        ]
        request(path: "auth/signIn", parameters: parameters, completion: completion)
    }
}
