//
//  AuthViewController.swift
//  Broccoli
//
//  Created by Fedor Emelianov on 28.08.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import Alamofire

class AuthViewController: UIViewController {
    @IBOutlet weak var login: FormTextField!
    @IBOutlet weak var password: FormTextField! 
    @IBOutlet weak var enter: UIButton!
    
    var keyboardShown: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        enter.isEnabled = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AuthViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func textChanged(_ sender: Any) {
        enter.isEnabled = !(login.text?.isEmpty)! && !(password.text?.isEmpty)!
    }
    
    @IBAction func enter(_ sender: Any) {
        JustHUD.shared.showInView(view: view)
        enter.isEnabled = false
        
        AuthManager.sharedInstance.auth(login: login.text!, password: password.text!, completion: {
            (response, message) -> Void in
            JustHUD.shared.hide()
            self.enter.isEnabled = true
            if((response) != nil) {
                self.notify(message: (response?.description)!, success: true)
            } else {
                self.notify(message: message!, success: false)
            }
        })
    }
    
    @IBAction func registerWithoutRegistration(_ sender: Any) {
        
        let tabBarController = ESTabBarController()
        let v1 = UIViewController()
        let v2 = MainViewController(nibName: "MainViewController", bundle: nil)
        let v3 = UIViewController()
        let v4 = UIViewController()
        let v5 = UIViewController()
        
        
        let c1 = ExampleBasicContentView()
        c1.highlightIconColor = UIColor(netHex: 0x8cad0f)
        c1.highlightTextColor = UIColor(netHex: 0x8cad0f)
        let c2 = ExampleBasicContentView()
        c2.highlightIconColor = UIColor(netHex: 0x8cad0f)
        c2.highlightTextColor = UIColor(netHex: 0x8cad0f)
        let c3 = ExampleBasicContentView()
        c3.highlightIconColor = UIColor(netHex: 0x8cad0f)
        c3.highlightTextColor = UIColor(netHex: 0x8cad0f)
        let c4 = ExampleBasicContentView()
        c4.highlightIconColor = UIColor(netHex: 0x8cad0f)
        c4.highlightTextColor = UIColor(netHex: 0x8cad0f)
        let c5 = ExampleBasicContentView()
        c5.highlightIconColor = UIColor(netHex: 0x8cad0f)
        c5.highlightTextColor = UIColor(netHex: 0x8cad0f)
        
        v1.tabBarItem = ESTabBarItem.init(c1, title: "Профиль", image: #imageLiteral(resourceName: "Human"), selectedImage: UIImage(named: "home_1"))
        v2.tabBarItem = ESTabBarItem.init(c2, title: "Главная", image: #imageLiteral(resourceName: "house"), selectedImage: UIImage(named: "find_1"))
        v3.tabBarItem = ESTabBarItem.init(c3, title: nil, image: #imageLiteral(resourceName: "green_search"), selectedImage: #imageLiteral(resourceName: "green_search"))
        v4.tabBarItem = ESTabBarItem.init(c4, title: "Корзина", image: #imageLiteral(resourceName: "cart-1"), selectedImage: UIImage(named: "favor_1"))
        v5.tabBarItem = ESTabBarItem.init(c5, title: "Хочу!", image: #imageLiteral(resourceName: "MagicWand"), selectedImage: UIImage(named: "me_1"))
        
        
        tabBarController.viewControllers = [v1, v2, v3, v4, v5]
        
        present(tabBarController, animated: true, completion: nil)
    }
    
    @IBAction func register(_ sender: Any) {
        self.performSegue(withIdentifier: "showRegister", sender: self)
    }
    
    func notify(message: String, success: Bool) {
        let alert = UIAlertController(title: success ? "Успешно":"Ошибка", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func keyboardWillShow(sender: NSNotification) {
        if(!keyboardShown) {
            keyboardShown = true
            self.view.frame.origin.y = -150
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        if(keyboardShown) {
            keyboardShown = false
            self.view.frame.origin.y = 0
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
