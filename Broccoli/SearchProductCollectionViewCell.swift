//
//  SearchProductCollectionViewCell.swift
//  Broccoli
//
//  Created by Quaka on 19.09.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class SearchProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
