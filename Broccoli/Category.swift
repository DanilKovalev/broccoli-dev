//
//  Category.swift
//  Broccoli
//
//  Created by Hadevs on 18/09/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Category {
    var id: String
    var active: Bool
    var sort: Int?
    var name: String
    var color: String? // in HEX
    var icon: String? // Link
    var childs: [Int] = []
    var products: [Product] = []
    
    var dict: [String: Any?] {
        return [
            "id": id,
            "active": active,
            "sort": sort,
            "name": name,
            "color": color,
            "icon": icon,
            "childs": childs,
            "products": products.map{$0.dict}
        ]
    }
    
    static func from(JSON json: JSON) -> Category {
        return Category(id: json["id"].stringValue,
                        active: json["active"].boolValue,
                        sort: json["sort"].int,
                        name: json["name"].stringValue,
                        color: json["color"].string,
                        icon: json["icon"].string,
                        childs: (json["childs"].arrayObject as? [Int]) ?? [],
                        products: json["products"].map{Product.from(JSON: $0.1)})
    }
}
