//
//  SelectCategoryTableViewCell.swift
//  Broccoli
//
//  Created by 1 on 19/09/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class SelectCategoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var colorCircle:   UIView!
    @IBOutlet weak var categoryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with category: Category) {
        categoryLabel.text = category.name.capitalized
        colorCircle.backgroundColor = UIColor(hex: (category.color ?? "").isEmpty ? "#f1f1f1" : category.color!)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        colorCircle.layer.masksToBounds = true
        colorCircle.layer.cornerRadius = colorCircle.frame.width / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
