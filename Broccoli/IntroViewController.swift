//
//  ViewController.swift
//  Broccoli
//
//  Created by Oleg Chebotarev on 28.08.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController, ViewPagerDataSource {

    @IBOutlet weak var pager: ViewPager!
    
    override func viewDidLoad() {
        super.viewDidLoad() 
        pager.dataSource = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(Store.sharedInstance.getBool(key: "introShown")) {
            self.performSegue(withIdentifier: "showAuth", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfItems(viewPager: ViewPager) -> Int {
        return 5
    }
    
    func viewAtIndex(viewPager: ViewPager, index: Int, view: UIView?) -> UIView {
        let view = IntroView()
        view.onNext = {
            (last: Bool) -> Void in
            if(last) {
                Store.sharedInstance.saveBool(key: "introShown", value: true)
                self.performSegue(withIdentifier: "showAuth", sender: self)
            } else {
                self.pager.moveToNextPage()
            }
        }
        switch index {
        case 0:
            view.set(titleText: "Поиск товара", mainText: "Все категории магазина как на ладони", viewIcon: "img_intro_ic_search", viewImage: "img_intro_search", isLast: false)
            break
        case 1:
            view.set(titleText: "Хочу!", mainText: "Добавляйте товары в Список желаний \"Хочу\" и получайте скидку на них после появления в нашем магазине", viewIcon: "img_intro_ic_want", viewImage: "img_intro_want", isLast: false)
            break
        case 2:
            view.set(titleText: "Профиль", mainText: "Следите за выполнением заказа онлайн в разделе \"Мои заказы\"", viewIcon: "img_intro_ic_profile", viewImage: "img_intro_profile", isLast: false)
            break
        case 3:
            view.set(titleText: "Любимые товары", mainText: "Добавляйте продукты в список любимых товаров", viewIcon: "img_intro_ic_fav", viewImage: "img_intro_fav", isLast: false)
            break
        case 4:
            view.set(titleText: "Профиль", mainText: "Делайте покупки, получайте баллы и получайте специальные предложения для постоянных клиентов", viewIcon: "img_intro_ic_profile", viewImage: "img_intro_bonuses", isLast: true)
            break
            default: break
            
        }
        return view
    }
    
    

}

