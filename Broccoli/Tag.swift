//
//  Tag.swift
//  Broccoli
//
//  Created by Hadevs on 04/10/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import SwiftyJSON

class Tag: NSObject {
    var id: Int
    var active: Bool
    var name: String
    var color: String? // in hex
    
    init(id: Int, active: Bool, name: String, color: String = "") {
        self.id     = id
        self.active = active
        self.name   = name
        self.color  = color
    }
    
    var dict: [String: Any] {
        return [
            "id": id,
            "active": active,
            "name": name,
            "color": color ?? ""
        ]
    }
    
    static func from(_ json: JSON) -> Tag {
        return Tag(id: json["id"].intValue,
                   active: json["active"].boolValue,
                   name: json["name"].stringValue,
                   color: json["color"].stringValue)
    }
}
