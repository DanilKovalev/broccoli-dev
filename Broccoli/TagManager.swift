//
//  TagManager.swift
//  Broccoli
//
//  Created by Hadevs on 04/10/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TagManager: BaseManager {
    static let sharedInstance = TagManager()
    
    var tags: [Tag] = []
    
    func loadSavedTags() {
        guard let tags = defaults.object(forKey: "tags") as? [[String: Any]] else {
            return
        }
        
        self.tags = tags.map{Tag.from(JSON($0))}
    }
    
    func id(by name: String) -> Int {
        return tags.filter {
            return $0.name == name
        }[0].id
    }
    
    func getAllTagNames() -> [String] {
        return tags.map{$0.name}
    }
    
    func saveTags() {
        defaults.set(tags.map{$0.dict}, forKey: "tags")
        defaults.synchronize()
    }
    
    func loadAllTags(_ completion: @escaping () -> Void) {
        let headers = [
            "Authorization":"Basic ZGV2Ok9vUnJBYTIwMTde"
        ]
        
        let _ = Alamofire.request("https://test.freshbroccoli.ru/api/v1/dictionaries/products/tags", method: .get, headers: headers)
            .responseSwiftyJSON { (request, response, json, error) in
                if error != nil {
                    completion()
                } else {
                    let tags = json.map{Tag.from($0.1)}
                    self.tags = tags
                    
                    if self.tags.isEmpty == false {
                        self.saveTags()
                    }
                    
                    completion()
                }
        }
    }

}
