//
//  RegisterViewController.swift
//  Broccoli
//
//  Created by Fedor Emelianov on 01.09.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var checkBox: CheckBox!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var name: UITextField!
    
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var clear: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUnderlines()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AuthViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func textChanged(_ sender: Any) {
        registerButton.isEnabled = (email.text != "") && (phone.text != "") && (name.text != "") && (password.text != "")
    }
    
    func setUnderlines() {
//        email.useUnderline()
//        phone.useUnderline()
//        name.useUnderline()
//        lastName.useUnderline()
//        password.useUnderline()
    } 
    @IBAction func dismissRegister(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func register(_ sender: Any) {
        if(!checkBox.isChecked) {
            alert(message: "Необходимо принять условия")
            return
        }
        JustHUD.shared.showInView(view: view)
        registerButton.isEnabled = false
        clear.isEnabled = false
        RegisterManager.sharedInstance.register(firstName: name.text!, lastName: lastName.text!, phone: phone.text!, email: email.text!, password: password.text!, completion: {
            (response, message) -> Void in
            JustHUD.shared.hide()
            self.registerButton.isEnabled = true
            self.clear.isEnabled = true
            if(response != nil) { 
                self.performSegue(withIdentifier: "showConfirm", sender: self)
            } else {
                self.alert(message: message!)
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "showConfirm") {
            if let vc = segue.destination as? ConfirmCodeViewController {
                vc.phone = phone.text!
            }
            
        }
    }
    
    func alert(message: String) {
        let alertController = UIAlertController(title: "Внимание", message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func clearFields(_ sender: Any) {
        phone.text = nil;
        lastName.text = nil;
        name.text = nil;
        email.text = nil;
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

}
