//
//  Catalog.swift
//  Broccoli
//
//  Created by Hadevs on 18/09/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProductManager: BaseManager {
    
    static let sharedInstance = ProductManager()
    
    func loadCategories(withCompletion completion: @escaping ([Category]) -> Void) {
        let headers = [
            "Authorization":"Basic ZGV2Ok9vUnJBYTIwMTde"
        ]
        
        let _ = Alamofire.request(baseUrl() + "categories", method: .get, headers: headers)
            .responseSwiftyJSON { (request, response, json, error) in
                if error != nil {
                    completion([])
                } else {
                    completion(json.map{Category.from(JSON: $0.1)})
                }
        }
    }
    
    func loadProductsFromServer(withCompletion completion: @escaping () -> Void, withOffset offset: Int = 0, andLimit limit: Int = 500) {
        
        let headers = [
            "Authorization":"Basic ZGV2Ok9vUnJBYTIwMTde"
        ]
        var url: String {
            return baseUrl() + "products" + "?offset=\(offset)" + "&limit=\(limit)"
        }
        
        let _ = Alamofire.request(url, method: .get, headers: headers)
            
            .responseSwiftyJSON { (request, response, json, error) in
                if error != nil {
                    completion()
                } else {
                    
                    let products = json.map{Product.from(JSON: $0.1)}
                    if products.isEmpty {
                        completion()
                    } else {
                        mainProducts += products
                        
                        if mainProducts.count > 0 {
                            ProductManager.sharedInstance.saveToDevice(products: mainProducts, forKey: "mainProducts")
                        }
                        self.loadProductsFromServer(withCompletion: completion, withOffset: offset + 501, andLimit: limit + 500)
                    }
                }
        }
    }
    
    func saveToDevice(products: [Product], forKey key: String) {
        defaults.set(products.map{$0.dict}, forKey: key)
        defaults.synchronize()
    }
    
    func loadProducts(fromKey key: String) -> [Product] {
        guard let products = defaults.object(forKey: key) as? [[String: Any?]] else {
            return []
        }
        
        return products.map { Product.from(JSON: JSON($0)) }
    }
}
