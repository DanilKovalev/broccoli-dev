//
//  ConfirmCodeViewController.swift
//  Broccoli
//
//  Created by Fedor Emelianov on 01.09.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class ConfirmCodeViewController: UIViewController {
    @IBOutlet weak var confirmButton: UIButton! 
    @IBOutlet weak var confirmCode: UITextField!
    
    var phone: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        confirmCode.useUnderline()
        confirmButton.isEnabled = false
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AuthViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    @IBAction func confirm(_ sender: Any) {
        JustHUD.shared.showInView(view: view)
        confirmButton.isEnabled = false
        RegisterManager.sharedInstance.confirm(phone: phone, code: confirmCode.text!, completion: {
            (response, message) in
            JustHUD.shared.hide()
            if(response != nil) {
                self.alert(message: "Код принят")
            } else {
                self.alert(message: message!)
            }
        })
    }
    
    
    func alert(message: String) {
        let alertController = UIAlertController(title: "Внимание", message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func textChanged(_ sender: Any) {
        confirmButton.isEnabled = confirmCode.text?.characters.count == 5
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
