//
//  AdImageTableViewCell.swift
//  Broccoli
//
//  Created by 1 on 04/10/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import ImageSlideshow
class AdImageTableViewCell: UITableViewCell {
    @IBOutlet var slideshow: ImageSlideshow!
    var imageArray: [InputSource] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        configure(with: UtilityManager.shared.banners)
        
        NotificationCenter.default.addObserver(self, selector: #selector(bannersUpdated), name: Notification.Name.init("bannersLoaded"), object: nil)
    }
    
    func bannersUpdated() {
        configure(with: UtilityManager.shared.banners)
    }

    func configure(with banners: [Banner]) {
        slideshow.backgroundColor = UIColor.white
        slideshow.slideshowInterval = 5.0
        slideshow.pageControlPosition = PageControlPosition.hidden
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideshow.activityIndicator = DefaultActivityIndicator()
        for banner in banners {
            let url = banner.detailPicture ?? ""
            print(url)
            let imageFromURL = KingfisherSource(urlString: url)
            imageArray.append(imageFromURL!)
        }
        slideshow.setImageInputs(
            imageArray
            )
    }

    
//
//    fileprivate func loadPhoto(by url: URL, withCompletion completion: @escaping (UIImage?) -> Void) {
//
//        var request = URLRequest(url: url)
//        request.setValue("Basic ZGV2Ok9vUnJBYTIwMTde", forHTTPHeaderField: "Authorization")
//
//        URLSession.shared.dataTask(with: request) { data, response, error in
//            if data != nil, let photo = UIImage(data: data!) {
//                DispatchQueue.main.async {
//                    completion(photo)
//                }
//            } else {
//                DispatchQueue.main.async {
//                    completion(nil)
//                }
//            }
//        }.resume()
//    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
