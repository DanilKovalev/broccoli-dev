//
//  ExtraCategoryTableViewCell.swift
//  Broccoli
//
//  Created by 1 on 05/10/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class ExtraCategoryTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var extraCategoryCollectionView: UICollectionView!
    
    var promos: [Promo] = [] {
        didSet {
            extraCategoryCollectionView.reloadData()
        }
    }
    
    var promoSelected: ((CatalogViewController) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCategoriesCollectionViewLayout()
        registeringCells()
        addObservers()
        
        promos = UtilityManager.shared.promos
        
        extraCategoryCollectionView.dataSource = self
        extraCategoryCollectionView.delegate = self
        // Initialization code
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(promosUpdated), name: Notification.Name.init("promosLoaded"), object: nil)
    }
    
    func promosUpdated() {
        promos = UtilityManager.shared.promos
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {	
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExtraCategoryCollectionViewID", for: indexPath) as! ExtraCategoryCollectionViewCell
        cell.layer.cornerRadius = 6
        let promo = promos[indexPath.row]
        cell.nameLabel.text = promo.name
        let filteredProducts = promo.products.sorted{PriceManager.sharedInstance.getPrice(for: $0, andCount: 0) ?? 0 > PriceManager.sharedInstance.getPrice(for: $1, andCount: 0) ?? 0}
        cell.priceLabel.text = "от \(filteredProducts.isEmpty ? 0 : PriceManager.sharedInstance.getPrice(for: filteredProducts[0], andCount: 0) ?? 0) р."
        
        if let url = URL(string: promo.detailPicture) {
            loadPhoto(by: url, withCompletion: { (image) in
                cell.iconView.image = image
            })
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let catalogVC = CatalogViewController(nibName: "CatalogViewController", bundle: nil)
        catalogVC.set(products: promos[indexPath.row].products)

        if let completion = promoSelected {
            completion(catalogVC)
        }
    }
    
    fileprivate func loadPhoto(by url: URL, withCompletion completion: @escaping (UIImage?) -> Void) {
        
        var request = URLRequest(url: url)
        request.setValue("Basic ZGV2Ok9vUnJBYTIwMTde", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            if data != nil, let photo = UIImage(data: data!) {
                DispatchQueue.main.async {
                    completion(photo)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }.resume()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return promos.count
    }
    
    func registeringCells() {
        let nib = UINib(nibName: "ExtraCategoryCollectionViewCell", bundle: nil)
        extraCategoryCollectionView.register(nib, forCellWithReuseIdentifier: "ExtraCategoryCollectionViewID")
    }
    
    fileprivate func configureCategoriesCollectionViewLayout() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 140, height: 140)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 25
        extraCategoryCollectionView.collectionViewLayout = layout
        extraCategoryCollectionView.showsHorizontalScrollIndicator = false
    }
        // Configure the view for the selected state
    
}
