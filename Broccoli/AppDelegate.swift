//
//  AppDelegate.swift
//  Broccoli
//
//  Created by Fedor Emelianov on 25.08.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import NotificationCenter
import Fabric
import Crashlytics


var mainProducts: [Product] = [] {
    didSet {
        NotificationCenter.default.post(.init(name: Notification.Name(rawValue: "productsUpdated")))
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Fabric.with([Crashlytics.self])
        
        DispatchQueue.global(qos: .utility).async {
            self.loadData()
        }
      
        return true
    }
    
    func loadData() {
        PriceManager.sharedInstance.loadSavedPrices()
        TagManager.sharedInstance.loadSavedTags()
        CategoryManager.sharedInstance.loadSavedCategories()
        UtilityManager.shared.loadSavedBanners()
        UtilityManager.shared.loadSavedPromos()
        
        UtilityManager.shared.loadPromos {
            DispatchQueue.main.async {
                let notification = Notification(name: NSNotification.Name.init("promosLoaded"), object: nil)
                NotificationCenter.default.post(notification)
            }
        }
        
        UtilityManager.shared.loadBanners {
            DispatchQueue.main.async {
                let notification = Notification(name: NSNotification.Name.init("bannersLoaded"), object: nil)
                NotificationCenter.default.post(notification)
            }
        }
        
        CategoryManager.sharedInstance.loadAllCategories {
            DispatchQueue.main.async {
                let notification = Notification(name: NSNotification.Name.init("categoriesLoaded"), object: nil)
                NotificationCenter.default.post(notification)
            }
        }
        
        TagManager.sharedInstance.loadAllTags {
            DispatchQueue.main.async {
                let notification = Notification(name: NSNotification.Name.init("tagsLoaded"), object: nil)
                NotificationCenter.default.post(notification)
            }
        }
        
        PriceManager.sharedInstance.loadAllPrices(withCompletion: {
            DispatchQueue.main.async {
                let notification = Notification(name: NSNotification.Name.init("pricesLoaded"), object: nil)
                NotificationCenter.default.post(notification)
            }
        })
        
        self.loadProducts()
    }
    
    func loadProducts() {
        mainProducts = ProductManager.sharedInstance.loadProducts(fromKey: "mainProducts")
        
        ProductManager.sharedInstance.loadProductsFromServer(withCompletion: {
            DispatchQueue.main.async {
                print("Downloaded \(mainProducts.count) products.")
            }
        })
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

