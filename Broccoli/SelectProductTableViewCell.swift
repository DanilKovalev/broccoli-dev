//
//  SelectProductTableViewCell.swift
//  Broccoli
//
//  Created by 1 on 19/09/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class SelectProductTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with category: Category) {
        titleLabel.text = category.name.capitalized
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
