//
//  IntroView.swift
//  Broccoli
//
//  Created by Oleg Chebotarev on 28.08.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class IntroView: UIView {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet var contentView: IntroView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var text: UILabel!
    
    var onNext: ((Bool) -> ())!
    var last: Bool!
    
    @IBAction func next(_ sender: Any) {
        if nil != onNext {
            onNext(last)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aCoder: NSCoder) {
        super.init(coder: aCoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("IntroView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func set(titleText: String, mainText: String, viewIcon: String, viewImage: String, isLast: Bool) {
        last = isLast
        title.text = titleText
        text.text = mainText
        icon.image = UIImage(named: viewIcon)
        image.image = UIImage(named: viewImage)
        
    }
}
