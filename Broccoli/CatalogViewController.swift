//
//  CatalogViewController.swift
//  Broccoli
//
//  Created by Nikita on 18/09/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class CatalogViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate {

    
    @IBOutlet weak var catalogCollectionView: UICollectionView!
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var tagsCollectionView: UICollectionView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var filtersLabel: UILabel!
    @IBOutlet weak var alphabetButton: UIButton!
    @IBOutlet weak var ratingButton: UIButton!
    @IBOutlet weak var priceButton: UIButton!
    
    let cellIdentifier              = "catalogCollectionCellIdentifier"
    
    var staticProducts: [Product] = []
    var tags: [String] = []
    var products: [Product]         = [] {
        didSet
        {
            if products.count > 0 { JustHUD.shared.hide() }
        }
    }
    var savedProducts: [Product]    = []
    var savedProductsBool           = true
    var isPaginationEnabled         = false
    var tagSavedProducts            = false
    var chosenTags: [Int] = [] {
        didSet {
            self.products = savedProducts
            if !chosenTags.isEmpty {
                self.products = products.filter {
                var result = false
                for tag in chosenTags {
                    if $0.tags.contains(tag) {
                        result = true
                    }
                }
                return result
            }
        }
            catalogCollectionView.reloadData()
        }
    }
    var isSearching: Bool {
        return !searchTextField.text!.isEmpty
    }
    
    @IBAction func filterAlphabetick(_ sender: Any) {
        ratingButton.setImage(#imageLiteral(resourceName: "up_gray_star"), for: .normal)
        priceButton.setImage(#imageLiteral(resourceName: "up_gray_rouble"), for: .normal)
        if alphabetButton.imageView?.image == #imageLiteral(resourceName: "up_gray_a"){
            alphabetButton.setImage(#imageLiteral(resourceName: "up_green_a"), for: .normal)
            self.products = products.sorted  {
                return String(describing: $0.name) < String(describing: $1.name)
            }
        }  else if alphabetButton.imageView?.image == #imageLiteral(resourceName: "up_green_a") {
            alphabetButton.setImage(#imageLiteral(resourceName: "down_green_a"), for: .normal)
            self.products = products.sorted  {
                return String(describing: $0.name) > String(describing: $1.name)
            }
        } else {
            alphabetButton.setImage(#imageLiteral(resourceName: "up_gray_a"), for: .normal)
            self.products = products.sorted  {
                return $0.sort! < $1.sort!
            }}
        catalogCollectionView.reloadData()
    }
    
    @IBAction func filterRating(_ sender: Any) {
        alphabetButton.setImage(#imageLiteral(resourceName: "up_gray_a"), for: .normal)
        priceButton.setImage(#imageLiteral(resourceName: "up_gray_rouble"), for: .normal)
        if ratingButton.imageView?.image == #imageLiteral(resourceName: "up_gray_star") {
            ratingButton.setImage(#imageLiteral(resourceName: "up_green_star"), for: .normal)
            self.products = products.sorted { $0.rating ?? 0 < $1.rating ?? 0 }
        } else if ratingButton.imageView?.image == #imageLiteral(resourceName: "up_green_star"){
            ratingButton.setImage(#imageLiteral(resourceName: "down_green_star"), for: .normal)
            self.products = products.sorted { $0.rating ?? 0 > $1.rating ?? 0 }
        } else {
            ratingButton.setImage(#imageLiteral(resourceName: "up_gray_star"), for: .normal)
            self.products = products.sorted { String(describing: $0.sort) < String(describing: $1.sort) }
        }
        catalogCollectionView.reloadData()
    }
    @IBAction func filterPrice(_ sender: Any) {
        alphabetButton.setImage(#imageLiteral(resourceName: "up_gray_a"), for: .normal)
         ratingButton.setImage(#imageLiteral(resourceName: "up_gray_star"), for: .normal)
        if priceButton.imageView?.image == #imageLiteral(resourceName: "up_gray_rouble") {
            priceButton.setImage(#imageLiteral(resourceName: "up_green_rouble"), for: .normal)
            self.products = products.sorted  {
                return PriceManager.sharedInstance.getPrice(for: $0, andCount: 0) ?? 0 < PriceManager.sharedInstance.getPrice(for: $1, andCount: 0) ?? 0
            }
        } else if priceButton.imageView?.image == #imageLiteral(resourceName: "up_green_rouble") {
            priceButton.setImage(#imageLiteral(resourceName: "down_green_rouble"), for: .normal)
            self.products = products.sorted  {
                return PriceManager.sharedInstance.getPrice(for: $0, andCount: 0) ?? 0 > PriceManager.sharedInstance.getPrice(for: $1, andCount: 0) ?? 0
            }
        } else {
            priceButton.setImage(#imageLiteral(resourceName: "up_gray_rouble"), for: .normal)
            self.products = products.sorted  {
                return String(describing: $0.sort) < String(describing: $1.sort)
            }
        }
        catalogCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
            case tagsCollectionView:
                
                guard let cell = collectionView.cellForItem(at: indexPath) as? SearchCollectionViewCell else {
                    return
                }
                cell.selectCell(!cell.isCustomSelected)
                let newTag = TagManager.sharedInstance.id(by:tags[indexPath.row])
                
                if chosenTags.isEmpty {
                    if tagSavedProducts {
                        products = savedProducts
                        tagSavedProducts = false
                    } else {
                        savedProducts = products
                        tagSavedProducts = true
                    }
                }
                
                if chosenTags.contains(newTag){
                    products = savedProducts
                    chosenTags.remove(at: chosenTags.index(of: newTag)!)
                } else {
                    chosenTags.append(newTag)
                }
                
            default: break;
        }
    }
    
    fileprivate func delegating() {
        catalogCollectionView.delegate = self
        catalogCollectionView.dataSource = self
        tagsCollectionView.delegate = self
        tagsCollectionView.dataSource = self
        searchTextField.delegate = self
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let updatedText = textFieldText.replacingCharacters(in: range, with: string)
        
        if savedProductsBool {
            savedProducts = products
            savedProductsBool = false
        }
        
        searchBy(updatedText)
        
        return true
    }
    
    fileprivate func searchBy(_ text: String) {
        
        products = staticProducts.filter({ (product) -> Bool in
            return (product.name ?? "").lowercased().contains(text.lowercased())
        })
        if !savedProductsBool && text == "" {
            products = savedProducts
            savedProductsBool = true
        }
        catalogCollectionView.reloadData()
    }
    
    fileprivate func registerNibs() {
        tagsCollectionView.register(UINib(nibName:"SearchCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "searchCategoryCell")
        catalogCollectionView.register(UINib(nibName:"CatalogCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    fileprivate func addLeftView() {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "lens"))
        imageView.sizeToFit()
        imageView.frame.size.width += 30
        imageView.contentMode = .scaleAspectFit
        searchTextField.leftView = imageView
        searchTextField.leftViewMode = .always
    }
    
    fileprivate func configureFlowLayout() {
        let space: CGFloat = 3.0
        
        flowLayout.minimumInteritemSpacing = space
        flowLayout.minimumLineSpacing = space
        flowLayout.itemSize = CGSize(width: 186, height: 326)
    }
    
    fileprivate func configureCategoriesCollectionViewLayout() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 85, height: 30)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 25
        tagsCollectionView.collectionViewLayout = layout
        tagsCollectionView.showsHorizontalScrollIndicator = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        filtersLabel.cornerRadius = filtersLabel.frame.height / 2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tags = TagManager.sharedInstance.getAllTagNames()
        configureFlowLayout()
        addLeftView()
        delegating()
        registerNibs()
        addObservers()
        configureCategoriesCollectionViewLayout()
        
        catalogCollectionView.keyboardDismissMode = .interactive
        
        filtersLabel.layer.borderWidth = 1
        filtersLabel.layer.borderColor = UIColor(netHex:0x8cad0f).cgColor
        
        if isNeedToShow {
            JustHUD.shared.showInView(view: view)
        }
    }
    
    var isNeedToShow = true
    
    func set(products: [Product]) {
        staticProducts = products
        self.products = products
        JustHUD.shared.hide()
        isNeedToShow = false
    }
    
    @IBAction func backAreaClicked(sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(productsUpdated), name: NSNotification.Name(rawValue: "productsUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(tagsUpdated), name: NSNotification.Name(rawValue: "tagsUpdated"), object: nil)
    }
    
    func tagsUpdated() {
        tags = TagManager.sharedInstance.getAllTagNames()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func productsUpdated() {
        
        guard products.count == 0 else {
            return
        }
        
        var counter = 0
        
        for product in mainProducts {
            products.append(product)
            catalogCollectionView.reloadData()
            
            if isPaginationEnabled && counter == 9 { break; }
            counter += 1
        }
        
        JustHUD.shared.hide()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == catalogCollectionView {
            return products.count
        } else {
            return tags.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == catalogCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CatalogCollectionViewCell
            
            cell.layer.shouldRasterize = true
            cell.layer.rasterizationScale = UIScreen.main.scale
            
            let product = products[indexPath.row]
            cell.configureBy(product: product)
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchCategoryCell", for: indexPath) as! SearchCollectionViewCell
            cell.titleLabel.text = self.tags[indexPath.row]
            return cell
        }
    }
 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
