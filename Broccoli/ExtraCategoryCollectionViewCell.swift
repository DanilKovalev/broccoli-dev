//
//  ExtraCategoryCollectionViewCell.swift
//  Broccoli
//
//  Created by 1 on 05/10/2017.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class ExtraCategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
