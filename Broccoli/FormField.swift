//
//  FormField.swift
//  Broccoli
//
//  Created by Oleg Chebotarev on 05.09.17.
//  Copyright © 2017 broccoli. All rights reserved.
//

import UIKit

class FormField: UITextField {

    override var tintColor: UIColor! {
        
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        let startingPoint   = CGPoint(x: rect.minX, y: rect.maxY)
        let endingPoint     = CGPoint(x: rect.maxX, y: rect.maxY)
        
        let path = UIBezierPath()
        
        path.move(to: startingPoint)
        path.addLine(to: endingPoint)
        path.lineWidth = 1.0
        
        tintColor.setStroke()
        
        path.stroke()
    }

}
